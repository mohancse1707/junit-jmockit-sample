package com.test.jmockit.JMockitsample.restservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.jmockit.JMockitsample.models.User;
import com.test.jmockit.JMockitsample.service.UserService;

@RestController
@RequestMapping("app/rest/services/jmockit")
public class UserRestServices {

	
	@Autowired
	UserService userService;
	
	@RequestMapping(value="/user/loginuser",method=RequestMethod.GET)
	@ResponseBody
	public User getLoginUser()
	{
		User user = userService.getLoggedInUser();
		
		return user;
	} 
	
	@RequestMapping(value="/user/saveUser",method=RequestMethod.POST)
	public void createOrUpdateUser(@RequestBody User user)
	{
		userService.saveUser(user);
	} 
}
