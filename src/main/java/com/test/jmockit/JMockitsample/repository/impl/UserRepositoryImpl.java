package com.test.jmockit.JMockitsample.repository.impl;

import org.springframework.stereotype.Repository;

import com.test.jmockit.JMockitsample.models.User;
import com.test.jmockit.JMockitsample.repository.UserRepository;

@Repository("userRepository")
public class UserRepositoryImpl implements UserRepository {

	/**
	 * Not yet implemented.
	 * 
	 * @param User
	 * @return
	 */
	public User getLoggedInUser() {

		User user = new User();
		user.setUsername("admin");
		user.setPassword("test123");
		return user;
	}

	/**
	 * Not yet implemented.
	 * 
	 * @param User
	 * @return
	 */
	public boolean authenticate(User user) {
		return false;
	}

	/**
	 * Not yet implemented.
	 * 
	 * @param User
	 * @return
	 */
	public void saveUser(User user) {
		// TODO Auto-generated method stub

	}

	/**
	 * Not yet implemented.
	 * 
	 * @param User
	 * @return
	 */
	public boolean isUserExist(User user) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Not yet implemented.
	 * 
	 * @param User
	 * @return
	 */
	public int getUserCountByRole(String role) {
		// TODO Auto-generated method stub
		return 0;
	}

}
