package com.test.jmockit.JMockitsample.repository;

import org.springframework.stereotype.Repository;

import com.test.jmockit.JMockitsample.models.User;

@Repository
public interface UserRepository {

	public User getLoggedInUser();

	public int getUserCountByRole(String role);

	public boolean authenticate(User user);

	public void saveUser(User user);

	public boolean isUserExist(User user);
}
