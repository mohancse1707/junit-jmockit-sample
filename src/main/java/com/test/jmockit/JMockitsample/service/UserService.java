package com.test.jmockit.JMockitsample.service;

import com.test.jmockit.JMockitsample.models.User;

public interface UserService {

	public User getLoggedInUser();

	public int getUserCountByRole(String role);

	public boolean authenticate(User user);

	public void saveUser(User user);

	public boolean isUserExist(User user);
}
