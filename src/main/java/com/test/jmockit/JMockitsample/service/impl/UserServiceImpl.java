/**
 * 
 */
package com.test.jmockit.JMockitsample.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.jmockit.JMockitsample.models.User;
import com.test.jmockit.JMockitsample.repository.UserRepository;
import com.test.jmockit.JMockitsample.service.UserService;

/**
 * @author Mohan
 *
 */
@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	/**
	 * Not yet implemented.
	 * 
	 * @param User
	 * @return
	 */
	public boolean authenticate(User user) {
		return false;
	}

	/**
	 * Not yet implemented.
	 * 
	 * @param User
	 * @return
	 */
	public User getLoggedInUser() {

		User user = userRepository.getLoggedInUser();
		//userRepository.authenticate(user);
		//userRepository.isUserExist(user);
		return user;
	}

	/**
	 * Not yet implemented.
	 * 
	 * @param User
	 * @return
	 */
	public void saveUser(User user) {
		// TODO Auto-generated method stub

	}

	public int getUserCountByRole(String role) {
		int count = 99;
		try {

			// TODO Impl stub

		} catch (Exception e) {
			e.printStackTrace();
		}

		return count;
	}

	public boolean isUserExist(User user) {
		// TODO Auto-generated method stub
		return false;
	}

}
