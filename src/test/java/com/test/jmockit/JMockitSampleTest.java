package com.test.jmockit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.test.jmockit.JMockitsample.models.User;
import com.test.jmockit.JMockitsample.service.UserService;

import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;


@ContextConfiguration(locations = { "classpath:/ApplicationContext.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class JMockitSampleTest {

	
	
	@Mocked
	private UserService userService;
	
	
	private User user;
	
	@Before
	public void setup()
	{
		user = new User();
		
		userService = new MockUp<UserService>() {
			
			@Mock
			int getUserCountByRole(String userRole)
			{
				if(userRole.equals("admin")){
					return 1;
				}
				else{
					return -1;
				}
			}
			
		}.getMockInstance();
		
		new MockUp<User>() {
			@Mock
			String privateMethod(){
				return "Mocked private method";
			}
		};
	}

	@Test
	public void getUserListCount() {
		
		
		String userRole = "guest";
		int count = userService.getUserCountByRole(userRole);
		
		System.out.println("count of admin " + count);
		assertTrue(count == -1);
	}
	
	@Test
	public void testPrivateMethod()
	{
					
		String  userName = user.getPrivateUserName();
		
		System.out.println("User Name :: " + userName);
		
		assertEquals("User Name returned ","Mocked private method",userName);
		
	}	
}
