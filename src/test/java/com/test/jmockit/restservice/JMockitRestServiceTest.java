package com.test.jmockit.restservice;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.test.jmockit.JMockitsample.models.User;
import com.test.jmockit.JMockitsample.restservice.UserRestServices;
import com.test.jmockit.JMockitsample.service.UserService;

import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Verifications;

/**
 * @author Mohan
 *
 */
@ContextConfiguration(locations = { "classpath:/ApplicationContext.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class JMockitRestServiceTest {

	private MockMvc mockMvc;

	@Mocked
	UserService userService;

	UserRestServices userRestServices = new UserRestServices();;

	@Before
	public void setup() {

		new NonStrictExpectations() {
			{
				userService.getLoggedInUser();
				result = new User("guest", "test123");
			};
		};

		ReflectionTestUtils.setField(userRestServices, "userService", userService);

		this.mockMvc = MockMvcBuilders.standaloneSetup(userRestServices).build();
	}

	@Test
	public void testLoggedInUser() throws Exception {

		/*
		 * new Expectations() { { userService.getLoggedInUser(); result=new
		 * User("guest", "test123"); } };
		 */

		ResultActions action = mockMvc.perform(MockMvcRequestBuilders.get("/app/rest/services/jmockit/user/loginuser")
				.contentType(TestUtil.APPLICATION_JSON_UTF8));

		action.andExpect(status().isOk());
		action.andExpect(jsonPath("$.username").value("guest"));
		action.andExpect(jsonPath("$.password").value("test123"));

		new Verifications() {
			{
				userService.getLoggedInUser();
				times = 1;
			}
		};
	}

	@Test
	public void saveUser() throws Exception {

		ResultActions action = mockMvc.perform(MockMvcRequestBuilders.post("/app/rest/services/jmockit/user/saveUser")
				.content(TestUtil.convertObjectToJsonBytes(new User("guest", "test123")))
				.contentType(TestUtil.APPLICATION_JSON_UTF8));

		action.andExpect(status().isOk());

		new Verifications() {
			{
				userService.saveUser(withInstanceOf(User.class));
				times = 1;
			}
		};
	}
}
