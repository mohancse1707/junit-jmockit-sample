package com.test.jmockit;

import org.junit.Test;

import com.test.jmockit.JMockitsample.models.User;
import com.test.jmockit.JMockitsample.repository.UserRepository;
import com.test.jmockit.JMockitsample.service.UserService;
import com.test.jmockit.JMockitsample.service.impl.UserServiceImpl;

import mockit.FullVerifications;
import mockit.FullVerificationsInOrder;
import mockit.Injectable;
import mockit.NonStrictExpectations;
import mockit.Tested;
import mockit.Verifications;
import mockit.VerificationsInOrder;
import mockit.internal.expectations.TestOnlyPhase;

//@ContextConfiguration(locations = { "classpath:/ApplicationContext.xml" })
//@RunWith(SpringJUnit4ClassRunner.class)
public class JMockitVerificationTest {

	@Tested
	private UserService userService = new UserServiceImpl();
	
	@Injectable
	UserRepository userRepository;
	
	//@Autowired
	//private UserService userService;
	
	@Test
	public void testLoggedInUser() {

		
		new NonStrictExpectations() {
			{
				userRepository.getLoggedInUser();
				result = new User("guest", "test123");
			}
		};
		userService.getLoggedInUser();
		
		new Verifications() {
			{
				userRepository.getLoggedInUser();
				times=1;
			}
		};
		
/*		new FullVerifications() {
			
			{
				userRepository.getLoggedInUser();
				userRepository.authenticate(withInstanceOf(User.class));
				//userRepository.isUserExist(withInstanceOf(User.class));
			}
		};*/
		
/*		new VerificationsInOrder() {
			{
				userRepository.getLoggedInUser();
				userRepository.authenticate(withInstanceOf(User.class));
			}
		};*/
		
/*		new FullVerificationsInOrder() {
			{
				userRepository.getLoggedInUser();
				userRepository.authenticate(withInstanceOf(User.class));
				//userRepository.isUserExist(withInstanceOf(User.class));
			}
		};*/
	}
}
