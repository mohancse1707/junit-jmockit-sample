package com.test.jmockit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import mockit.FullVerifications;
import mockit.Mocked;
import mockit.Verifications;
import mockit.VerificationsInOrder;
import mockit.internal.expectations.TestOnlyPhase;

@ContextConfiguration(locations = { "classpath:/ApplicationContext.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class VerifyInternalMethodsTest {

	@Mocked
	User user;

	static class User {

		String name;
		int age;

		public void setName(String name) {
			this.name = name;
		}

		public void setAge(int age) {
			this.age = age;
		}
	}

	static class UserService {

		void populateUser() {
			User admin = new User();
			admin.setName("admin");
			admin.setAge(30);

			User guest = new User();
			guest.setName("guest");
			guest.setAge(31);
			
		}

	}

	@Test
	public void verifyInternalMethods() {

		new UserService().populateUser(); // replay phase

		
		/*		new Verifications() { 			// verify phase
			{
				User admin = new User();
				admin.setName(anyString);
				admin.setAge(anyInt);
			}
		};*/

		
		/*new FullVerifications() {

			{
				User admin = new User();
				admin.setName(anyString);
				admin.setAge(anyInt);
			}
		};*/
		


		
		
	}

}
