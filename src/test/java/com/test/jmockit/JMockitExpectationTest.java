
package com.test.jmockit;

import org.junit.Assert;
import org.junit.Test;

import com.test.jmockit.JMockitsample.models.User;
import com.test.jmockit.JMockitsample.repository.UserRepository;
import com.test.jmockit.JMockitsample.service.UserService;
import com.test.jmockit.JMockitsample.service.impl.UserServiceImpl;

import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;

public class JMockitExpectationTest {

	@Tested
	private UserService userService = new UserServiceImpl();
	
	@Injectable
	UserRepository userRepository;
	
	
	@Test
	public void testUserServiceUserList() {
		new Expectations() {
			{
				userRepository.getLoggedInUser();
				result = new User("guest", "test123");
			}
		};
				
		
		User user = userService.getLoggedInUser();
		
		Assert.assertEquals("guest", user.getUsername());
		
	}
}
